/* tslint:disable */
/* eslint-disable */
/**
 * e-commerce-service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.74
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import { CatalogItemBaseDto } from './catalog-item-base-dto';

/**
 * 
 * @export
 * @interface CatalogListResponseDto
 */
export interface CatalogListResponseDto {
    /**
     * 
     * @type {number}
     * @memberof CatalogListResponseDto
     */
    'total': number;
    /**
     * 
     * @type {Array<CatalogItemBaseDto>}
     * @memberof CatalogListResponseDto
     */
    'data': Array<CatalogItemBaseDto>;
}

