//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class CatalogsApi {
  CatalogsApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Performs an HTTP 'GET /catalogs/{productId}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] productId (required):
  Future<Response> catalogControllerGetCatalogDetailWithHttpInfo(String productId,) async {
    // ignore: prefer_const_declarations
    final path = r'/catalogs/{productId}'
      .replaceAll('{productId}', productId);

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// Parameters:
  ///
  /// * [String] productId (required):
  Future<CatalogDetailResponseDto?> catalogControllerGetCatalogDetail(String productId,) async {
    final response = await catalogControllerGetCatalogDetailWithHttpInfo(productId,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'CatalogDetailResponseDto',) as CatalogDetailResponseDto;
    
    }
    return null;
  }

  /// Performs an HTTP 'GET /catalogs' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [num] pageIndex (required):
  ///
  /// * [num] pageSize (required):
  ///
  /// * [String] currentCountry (required):
  ///
  /// * [String] parentCategoryId:
  ///
  /// * [String] categoryId:
  ///
  /// * [String] keyword:
  ///
  /// * [List<String>] productIds:
  ///
  /// * [List<String>] categoryIds:
  ///
  /// * [List<String>] productTypes:
  ///
  /// * [String] sortBy:
  Future<Response> catalogControllerGetCatalogsWithHttpInfo(num pageIndex, num pageSize, String currentCountry, { String? parentCategoryId, String? categoryId, String? keyword, List<String>? productIds, List<String>? categoryIds, List<String>? productTypes, String? sortBy, }) async {
    // ignore: prefer_const_declarations
    final path = r'/catalogs';

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (parentCategoryId != null) {
      queryParams.addAll(_queryParams('', 'parentCategoryId', parentCategoryId));
    }
    if (categoryId != null) {
      queryParams.addAll(_queryParams('', 'categoryId', categoryId));
    }
    if (keyword != null) {
      queryParams.addAll(_queryParams('', 'keyword', keyword));
    }
    if (productIds != null) {
      queryParams.addAll(_queryParams('multi', 'productIds', productIds));
    }
    if (categoryIds != null) {
      queryParams.addAll(_queryParams('multi', 'categoryIds', categoryIds));
    }
    if (productTypes != null) {
      queryParams.addAll(_queryParams('multi', 'productTypes', productTypes));
    }
    if (sortBy != null) {
      queryParams.addAll(_queryParams('', 'sortBy', sortBy));
    }
      queryParams.addAll(_queryParams('', 'pageIndex', pageIndex));
      queryParams.addAll(_queryParams('', 'pageSize', pageSize));

    headerParams[r'current-country'] = parameterToString(currentCountry);

    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// Parameters:
  ///
  /// * [num] pageIndex (required):
  ///
  /// * [num] pageSize (required):
  ///
  /// * [String] currentCountry (required):
  ///
  /// * [String] parentCategoryId:
  ///
  /// * [String] categoryId:
  ///
  /// * [String] keyword:
  ///
  /// * [List<String>] productIds:
  ///
  /// * [List<String>] categoryIds:
  ///
  /// * [List<String>] productTypes:
  ///
  /// * [String] sortBy:
  Future<CatalogListResponseDto?> catalogControllerGetCatalogs(num pageIndex, num pageSize, String currentCountry, { String? parentCategoryId, String? categoryId, String? keyword, List<String>? productIds, List<String>? categoryIds, List<String>? productTypes, String? sortBy, }) async {
    final response = await catalogControllerGetCatalogsWithHttpInfo(pageIndex, pageSize, currentCountry,  parentCategoryId: parentCategoryId, categoryId: categoryId, keyword: keyword, productIds: productIds, categoryIds: categoryIds, productTypes: productTypes, sortBy: sortBy, );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'CatalogListResponseDto',) as CatalogListResponseDto;
    
    }
    return null;
  }
}
