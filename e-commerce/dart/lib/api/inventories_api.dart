//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class InventoriesApi {
  InventoriesApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Performs an HTTP 'GET /inventories/products/{productId}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] productId (required):
  Future<Response> productInventoryControllerGetProductInventoryDetailWithHttpInfo(String productId,) async {
    // ignore: prefer_const_declarations
    final path = r'/inventories/products/{productId}'
      .replaceAll('{productId}', productId);

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// Parameters:
  ///
  /// * [String] productId (required):
  Future<void> productInventoryControllerGetProductInventoryDetail(String productId,) async {
    final response = await productInventoryControllerGetProductInventoryDetailWithHttpInfo(productId,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'GET /inventories/products' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [num] pageIndex (required):
  ///
  /// * [num] pageSize (required):
  ///
  /// * [List<String>] productIds:
  Future<Response> productInventoryControllerGetProductInventoryListWithHttpInfo(num pageIndex, num pageSize, { List<String>? productIds, }) async {
    // ignore: prefer_const_declarations
    final path = r'/inventories/products';

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (productIds != null) {
      queryParams.addAll(_queryParams('multi', 'productIds', productIds));
    }
      queryParams.addAll(_queryParams('', 'pageIndex', pageIndex));
      queryParams.addAll(_queryParams('', 'pageSize', pageSize));

    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// Parameters:
  ///
  /// * [num] pageIndex (required):
  ///
  /// * [num] pageSize (required):
  ///
  /// * [List<String>] productIds:
  Future<void> productInventoryControllerGetProductInventoryList(num pageIndex, num pageSize, { List<String>? productIds, }) async {
    final response = await productInventoryControllerGetProductInventoryListWithHttpInfo(pageIndex, pageSize,  productIds: productIds, );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }
}
