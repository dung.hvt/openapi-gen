//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class MassUploadDto {
  /// Returns a new [MassUploadDto] instance.
  MassUploadDto({
    required this.id,
    required this.fileName,
    required this.createdAt,
    required this.status,
  });

  String id;

  String fileName;

  DateTime createdAt;

  MassUploadDtoStatusEnum status;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MassUploadDto &&
    other.id == id &&
    other.fileName == fileName &&
    other.createdAt == createdAt &&
    other.status == status;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (id.hashCode) +
    (fileName.hashCode) +
    (createdAt.hashCode) +
    (status.hashCode);

  @override
  String toString() => 'MassUploadDto[id=$id, fileName=$fileName, createdAt=$createdAt, status=$status]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'id'] = this.id;
      json[r'fileName'] = this.fileName;
      json[r'createdAt'] = this.createdAt.toUtc().toIso8601String();
      json[r'status'] = this.status;
    return json;
  }

  /// Returns a new [MassUploadDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static MassUploadDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "MassUploadDto[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "MassUploadDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return MassUploadDto(
        id: mapValueOfType<String>(json, r'id')!,
        fileName: mapValueOfType<String>(json, r'fileName')!,
        createdAt: mapDateTime(json, r'createdAt', r'')!,
        status: MassUploadDtoStatusEnum.fromJson(json[r'status'])!,
      );
    }
    return null;
  }

  static List<MassUploadDto> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <MassUploadDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = MassUploadDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, MassUploadDto> mapFromJson(dynamic json) {
    final map = <String, MassUploadDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = MassUploadDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of MassUploadDto-objects as value to a dart map
  static Map<String, List<MassUploadDto>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<MassUploadDto>>{};
    if (json is Map && json.isNotEmpty) {
      // ignore: parameter_assignments
      json = json.cast<String, dynamic>();
      for (final entry in json.entries) {
        map[entry.key] = MassUploadDto.listFromJson(entry.value, growable: growable,);
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'fileName',
    'createdAt',
    'status',
  };
}


class MassUploadDtoStatusEnum {
  /// Instantiate a new enum with the provided [value].
  const MassUploadDtoStatusEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const pending = MassUploadDtoStatusEnum._(r'pending');
  static const completed = MassUploadDtoStatusEnum._(r'completed');

  /// List of all possible values in this [enum][MassUploadDtoStatusEnum].
  static const values = <MassUploadDtoStatusEnum>[
    pending,
    completed,
  ];

  static MassUploadDtoStatusEnum? fromJson(dynamic value) => MassUploadDtoStatusEnumTypeTransformer().decode(value);

  static List<MassUploadDtoStatusEnum> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <MassUploadDtoStatusEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = MassUploadDtoStatusEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [MassUploadDtoStatusEnum] to String,
/// and [decode] dynamic data back to [MassUploadDtoStatusEnum].
class MassUploadDtoStatusEnumTypeTransformer {
  factory MassUploadDtoStatusEnumTypeTransformer() => _instance ??= const MassUploadDtoStatusEnumTypeTransformer._();

  const MassUploadDtoStatusEnumTypeTransformer._();

  String encode(MassUploadDtoStatusEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a MassUploadDtoStatusEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  MassUploadDtoStatusEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data) {
        case r'pending': return MassUploadDtoStatusEnum.pending;
        case r'completed': return MassUploadDtoStatusEnum.completed;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [MassUploadDtoStatusEnumTypeTransformer] instance.
  static MassUploadDtoStatusEnumTypeTransformer? _instance;
}


