//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class GetMassUploadDetailResDto {
  /// Returns a new [GetMassUploadDetailResDto] instance.
  GetMassUploadDetailResDto({
    required this.id,
    required this.status,
    this.massUploadItems = const [],
  });

  String id;

  GetMassUploadDetailResDtoStatusEnum status;

  List<MassUploadItemResDto> massUploadItems;

  @override
  bool operator ==(Object other) => identical(this, other) || other is GetMassUploadDetailResDto &&
    other.id == id &&
    other.status == status &&
    _deepEquality.equals(other.massUploadItems, massUploadItems);

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (id.hashCode) +
    (status.hashCode) +
    (massUploadItems.hashCode);

  @override
  String toString() => 'GetMassUploadDetailResDto[id=$id, status=$status, massUploadItems=$massUploadItems]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'id'] = this.id;
      json[r'status'] = this.status;
      json[r'massUploadItems'] = this.massUploadItems;
    return json;
  }

  /// Returns a new [GetMassUploadDetailResDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static GetMassUploadDetailResDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "GetMassUploadDetailResDto[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "GetMassUploadDetailResDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return GetMassUploadDetailResDto(
        id: mapValueOfType<String>(json, r'id')!,
        status: GetMassUploadDetailResDtoStatusEnum.fromJson(json[r'status'])!,
        massUploadItems: MassUploadItemResDto.listFromJson(json[r'massUploadItems']),
      );
    }
    return null;
  }

  static List<GetMassUploadDetailResDto> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <GetMassUploadDetailResDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = GetMassUploadDetailResDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, GetMassUploadDetailResDto> mapFromJson(dynamic json) {
    final map = <String, GetMassUploadDetailResDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = GetMassUploadDetailResDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of GetMassUploadDetailResDto-objects as value to a dart map
  static Map<String, List<GetMassUploadDetailResDto>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<GetMassUploadDetailResDto>>{};
    if (json is Map && json.isNotEmpty) {
      // ignore: parameter_assignments
      json = json.cast<String, dynamic>();
      for (final entry in json.entries) {
        map[entry.key] = GetMassUploadDetailResDto.listFromJson(entry.value, growable: growable,);
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'status',
    'massUploadItems',
  };
}


class GetMassUploadDetailResDtoStatusEnum {
  /// Instantiate a new enum with the provided [value].
  const GetMassUploadDetailResDtoStatusEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const pending = GetMassUploadDetailResDtoStatusEnum._(r'pending');
  static const completed = GetMassUploadDetailResDtoStatusEnum._(r'completed');

  /// List of all possible values in this [enum][GetMassUploadDetailResDtoStatusEnum].
  static const values = <GetMassUploadDetailResDtoStatusEnum>[
    pending,
    completed,
  ];

  static GetMassUploadDetailResDtoStatusEnum? fromJson(dynamic value) => GetMassUploadDetailResDtoStatusEnumTypeTransformer().decode(value);

  static List<GetMassUploadDetailResDtoStatusEnum> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <GetMassUploadDetailResDtoStatusEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = GetMassUploadDetailResDtoStatusEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [GetMassUploadDetailResDtoStatusEnum] to String,
/// and [decode] dynamic data back to [GetMassUploadDetailResDtoStatusEnum].
class GetMassUploadDetailResDtoStatusEnumTypeTransformer {
  factory GetMassUploadDetailResDtoStatusEnumTypeTransformer() => _instance ??= const GetMassUploadDetailResDtoStatusEnumTypeTransformer._();

  const GetMassUploadDetailResDtoStatusEnumTypeTransformer._();

  String encode(GetMassUploadDetailResDtoStatusEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a GetMassUploadDetailResDtoStatusEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  GetMassUploadDetailResDtoStatusEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data) {
        case r'pending': return GetMassUploadDetailResDtoStatusEnum.pending;
        case r'completed': return GetMassUploadDetailResDtoStatusEnum.completed;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [GetMassUploadDetailResDtoStatusEnumTypeTransformer] instance.
  static GetMassUploadDetailResDtoStatusEnumTypeTransformer? _instance;
}


