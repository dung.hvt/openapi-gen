//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CatalogItemBaseDto {
  /// Returns a new [CatalogItemBaseDto] instance.
  CatalogItemBaseDto({
    required this.id,
    required this.currency,
    required this.name,
    required this.mainImage,
    this.otherImages = const [],
    required this.basePrice,
    required this.organizationId,
    required this.organizationName,
    required this.organizationStatus,
    this.calculatedCampaigns = const [],
    required this.eVoucherTag,
    required this.productType,
  });

  String id;

  String currency;

  String name;

  String mainImage;

  List<String> otherImages;

  num basePrice;

  String organizationId;

  String organizationName;

  CatalogItemBaseDtoOrganizationStatusEnum organizationStatus;

  List<CalculatedCampaignResponseDto> calculatedCampaigns;

  Object eVoucherTag;

  CatalogItemBaseDtoProductTypeEnum productType;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CatalogItemBaseDto &&
    other.id == id &&
    other.currency == currency &&
    other.name == name &&
    other.mainImage == mainImage &&
    _deepEquality.equals(other.otherImages, otherImages) &&
    other.basePrice == basePrice &&
    other.organizationId == organizationId &&
    other.organizationName == organizationName &&
    other.organizationStatus == organizationStatus &&
    _deepEquality.equals(other.calculatedCampaigns, calculatedCampaigns) &&
    other.eVoucherTag == eVoucherTag &&
    other.productType == productType;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (id.hashCode) +
    (currency.hashCode) +
    (name.hashCode) +
    (mainImage.hashCode) +
    (otherImages.hashCode) +
    (basePrice.hashCode) +
    (organizationId.hashCode) +
    (organizationName.hashCode) +
    (organizationStatus.hashCode) +
    (calculatedCampaigns.hashCode) +
    (eVoucherTag.hashCode) +
    (productType.hashCode);

  @override
  String toString() => 'CatalogItemBaseDto[id=$id, currency=$currency, name=$name, mainImage=$mainImage, otherImages=$otherImages, basePrice=$basePrice, organizationId=$organizationId, organizationName=$organizationName, organizationStatus=$organizationStatus, calculatedCampaigns=$calculatedCampaigns, eVoucherTag=$eVoucherTag, productType=$productType]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'id'] = this.id;
      json[r'currency'] = this.currency;
      json[r'name'] = this.name;
      json[r'mainImage'] = this.mainImage;
      json[r'otherImages'] = this.otherImages;
      json[r'basePrice'] = this.basePrice;
      json[r'organizationId'] = this.organizationId;
      json[r'organizationName'] = this.organizationName;
      json[r'organizationStatus'] = this.organizationStatus;
      json[r'calculatedCampaigns'] = this.calculatedCampaigns;
      json[r'eVoucherTag'] = this.eVoucherTag;
      json[r'productType'] = this.productType;
    return json;
  }

  /// Returns a new [CatalogItemBaseDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static CatalogItemBaseDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "CatalogItemBaseDto[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "CatalogItemBaseDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return CatalogItemBaseDto(
        id: mapValueOfType<String>(json, r'id')!,
        currency: mapValueOfType<String>(json, r'currency')!,
        name: mapValueOfType<String>(json, r'name')!,
        mainImage: mapValueOfType<String>(json, r'mainImage')!,
        otherImages: json[r'otherImages'] is Iterable
            ? (json[r'otherImages'] as Iterable).cast<String>().toList(growable: false)
            : const [],
        basePrice: num.parse('${json[r'basePrice']}'),
        organizationId: mapValueOfType<String>(json, r'organizationId')!,
        organizationName: mapValueOfType<String>(json, r'organizationName')!,
        organizationStatus: CatalogItemBaseDtoOrganizationStatusEnum.fromJson(json[r'organizationStatus'])!,
        calculatedCampaigns: CalculatedCampaignResponseDto.listFromJson(json[r'calculatedCampaigns']),
        eVoucherTag: mapValueOfType<Object>(json, r'eVoucherTag')!,
        productType: CatalogItemBaseDtoProductTypeEnum.fromJson(json[r'productType'])!,
      );
    }
    return null;
  }

  static List<CatalogItemBaseDto> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CatalogItemBaseDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CatalogItemBaseDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, CatalogItemBaseDto> mapFromJson(dynamic json) {
    final map = <String, CatalogItemBaseDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CatalogItemBaseDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of CatalogItemBaseDto-objects as value to a dart map
  static Map<String, List<CatalogItemBaseDto>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<CatalogItemBaseDto>>{};
    if (json is Map && json.isNotEmpty) {
      // ignore: parameter_assignments
      json = json.cast<String, dynamic>();
      for (final entry in json.entries) {
        map[entry.key] = CatalogItemBaseDto.listFromJson(entry.value, growable: growable,);
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'currency',
    'name',
    'mainImage',
    'otherImages',
    'basePrice',
    'organizationId',
    'organizationName',
    'organizationStatus',
    'calculatedCampaigns',
    'eVoucherTag',
    'productType',
  };
}


class CatalogItemBaseDtoOrganizationStatusEnum {
  /// Instantiate a new enum with the provided [value].
  const CatalogItemBaseDtoOrganizationStatusEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const verified = CatalogItemBaseDtoOrganizationStatusEnum._(r'verified');
  static const unverified = CatalogItemBaseDtoOrganizationStatusEnum._(r'unverified');

  /// List of all possible values in this [enum][CatalogItemBaseDtoOrganizationStatusEnum].
  static const values = <CatalogItemBaseDtoOrganizationStatusEnum>[
    verified,
    unverified,
  ];

  static CatalogItemBaseDtoOrganizationStatusEnum? fromJson(dynamic value) => CatalogItemBaseDtoOrganizationStatusEnumTypeTransformer().decode(value);

  static List<CatalogItemBaseDtoOrganizationStatusEnum> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CatalogItemBaseDtoOrganizationStatusEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CatalogItemBaseDtoOrganizationStatusEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [CatalogItemBaseDtoOrganizationStatusEnum] to String,
/// and [decode] dynamic data back to [CatalogItemBaseDtoOrganizationStatusEnum].
class CatalogItemBaseDtoOrganizationStatusEnumTypeTransformer {
  factory CatalogItemBaseDtoOrganizationStatusEnumTypeTransformer() => _instance ??= const CatalogItemBaseDtoOrganizationStatusEnumTypeTransformer._();

  const CatalogItemBaseDtoOrganizationStatusEnumTypeTransformer._();

  String encode(CatalogItemBaseDtoOrganizationStatusEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a CatalogItemBaseDtoOrganizationStatusEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  CatalogItemBaseDtoOrganizationStatusEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data) {
        case r'verified': return CatalogItemBaseDtoOrganizationStatusEnum.verified;
        case r'unverified': return CatalogItemBaseDtoOrganizationStatusEnum.unverified;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [CatalogItemBaseDtoOrganizationStatusEnumTypeTransformer] instance.
  static CatalogItemBaseDtoOrganizationStatusEnumTypeTransformer? _instance;
}



class CatalogItemBaseDtoProductTypeEnum {
  /// Instantiate a new enum with the provided [value].
  const CatalogItemBaseDtoProductTypeEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const main = CatalogItemBaseDtoProductTypeEnum._(r'main');
  static const addons = CatalogItemBaseDtoProductTypeEnum._(r'addons');
  static const eCommerce = CatalogItemBaseDtoProductTypeEnum._(r'e_commerce');
  static const program = CatalogItemBaseDtoProductTypeEnum._(r'program');
  static const eVoucher = CatalogItemBaseDtoProductTypeEnum._(r'e_voucher');

  /// List of all possible values in this [enum][CatalogItemBaseDtoProductTypeEnum].
  static const values = <CatalogItemBaseDtoProductTypeEnum>[
    main,
    addons,
    eCommerce,
    program,
    eVoucher,
  ];

  static CatalogItemBaseDtoProductTypeEnum? fromJson(dynamic value) => CatalogItemBaseDtoProductTypeEnumTypeTransformer().decode(value);

  static List<CatalogItemBaseDtoProductTypeEnum> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CatalogItemBaseDtoProductTypeEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CatalogItemBaseDtoProductTypeEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [CatalogItemBaseDtoProductTypeEnum] to String,
/// and [decode] dynamic data back to [CatalogItemBaseDtoProductTypeEnum].
class CatalogItemBaseDtoProductTypeEnumTypeTransformer {
  factory CatalogItemBaseDtoProductTypeEnumTypeTransformer() => _instance ??= const CatalogItemBaseDtoProductTypeEnumTypeTransformer._();

  const CatalogItemBaseDtoProductTypeEnumTypeTransformer._();

  String encode(CatalogItemBaseDtoProductTypeEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a CatalogItemBaseDtoProductTypeEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  CatalogItemBaseDtoProductTypeEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data) {
        case r'main': return CatalogItemBaseDtoProductTypeEnum.main;
        case r'addons': return CatalogItemBaseDtoProductTypeEnum.addons;
        case r'e_commerce': return CatalogItemBaseDtoProductTypeEnum.eCommerce;
        case r'program': return CatalogItemBaseDtoProductTypeEnum.program;
        case r'e_voucher': return CatalogItemBaseDtoProductTypeEnum.eVoucher;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [CatalogItemBaseDtoProductTypeEnumTypeTransformer] instance.
  static CatalogItemBaseDtoProductTypeEnumTypeTransformer? _instance;
}


