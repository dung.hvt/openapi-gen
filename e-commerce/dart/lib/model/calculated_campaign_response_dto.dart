//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CalculatedCampaignResponseDto {
  /// Returns a new [CalculatedCampaignResponseDto] instance.
  CalculatedCampaignResponseDto({
    required this.campaignType,
    required this.discount,
    required this.discountPrice,
  });

  CalculatedCampaignResponseDtoCampaignTypeEnum campaignType;

  num discount;

  num discountPrice;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CalculatedCampaignResponseDto &&
    other.campaignType == campaignType &&
    other.discount == discount &&
    other.discountPrice == discountPrice;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (campaignType.hashCode) +
    (discount.hashCode) +
    (discountPrice.hashCode);

  @override
  String toString() => 'CalculatedCampaignResponseDto[campaignType=$campaignType, discount=$discount, discountPrice=$discountPrice]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'campaignType'] = this.campaignType;
      json[r'discount'] = this.discount;
      json[r'discountPrice'] = this.discountPrice;
    return json;
  }

  /// Returns a new [CalculatedCampaignResponseDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static CalculatedCampaignResponseDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "CalculatedCampaignResponseDto[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "CalculatedCampaignResponseDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return CalculatedCampaignResponseDto(
        campaignType: CalculatedCampaignResponseDtoCampaignTypeEnum.fromJson(json[r'campaignType'])!,
        discount: num.parse('${json[r'discount']}'),
        discountPrice: num.parse('${json[r'discountPrice']}'),
      );
    }
    return null;
  }

  static List<CalculatedCampaignResponseDto> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CalculatedCampaignResponseDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CalculatedCampaignResponseDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, CalculatedCampaignResponseDto> mapFromJson(dynamic json) {
    final map = <String, CalculatedCampaignResponseDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CalculatedCampaignResponseDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of CalculatedCampaignResponseDto-objects as value to a dart map
  static Map<String, List<CalculatedCampaignResponseDto>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<CalculatedCampaignResponseDto>>{};
    if (json is Map && json.isNotEmpty) {
      // ignore: parameter_assignments
      json = json.cast<String, dynamic>();
      for (final entry in json.entries) {
        map[entry.key] = CalculatedCampaignResponseDto.listFromJson(entry.value, growable: growable,);
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'campaignType',
    'discount',
    'discountPrice',
  };
}


class CalculatedCampaignResponseDtoCampaignTypeEnum {
  /// Instantiate a new enum with the provided [value].
  const CalculatedCampaignResponseDtoCampaignTypeEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const promoPrice = CalculatedCampaignResponseDtoCampaignTypeEnum._(r'promo_price');
  static const discountedDeliveryFee = CalculatedCampaignResponseDtoCampaignTypeEnum._(r'discounted_delivery_fee');

  /// List of all possible values in this [enum][CalculatedCampaignResponseDtoCampaignTypeEnum].
  static const values = <CalculatedCampaignResponseDtoCampaignTypeEnum>[
    promoPrice,
    discountedDeliveryFee,
  ];

  static CalculatedCampaignResponseDtoCampaignTypeEnum? fromJson(dynamic value) => CalculatedCampaignResponseDtoCampaignTypeEnumTypeTransformer().decode(value);

  static List<CalculatedCampaignResponseDtoCampaignTypeEnum> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CalculatedCampaignResponseDtoCampaignTypeEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CalculatedCampaignResponseDtoCampaignTypeEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [CalculatedCampaignResponseDtoCampaignTypeEnum] to String,
/// and [decode] dynamic data back to [CalculatedCampaignResponseDtoCampaignTypeEnum].
class CalculatedCampaignResponseDtoCampaignTypeEnumTypeTransformer {
  factory CalculatedCampaignResponseDtoCampaignTypeEnumTypeTransformer() => _instance ??= const CalculatedCampaignResponseDtoCampaignTypeEnumTypeTransformer._();

  const CalculatedCampaignResponseDtoCampaignTypeEnumTypeTransformer._();

  String encode(CalculatedCampaignResponseDtoCampaignTypeEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a CalculatedCampaignResponseDtoCampaignTypeEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  CalculatedCampaignResponseDtoCampaignTypeEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data) {
        case r'promo_price': return CalculatedCampaignResponseDtoCampaignTypeEnum.promoPrice;
        case r'discounted_delivery_fee': return CalculatedCampaignResponseDtoCampaignTypeEnum.discountedDeliveryFee;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [CalculatedCampaignResponseDtoCampaignTypeEnumTypeTransformer] instance.
  static CalculatedCampaignResponseDtoCampaignTypeEnumTypeTransformer? _instance;
}


