//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class MassUploadItemResDto {
  /// Returns a new [MassUploadItemResDto] instance.
  MassUploadItemResDto({
    required this.id,
    required this.status,
    required this.priority,
    required this.value,
    required this.reason,
  });

  String id;

  MassUploadItemResDtoStatusEnum status;

  num priority;

  Object value;

  Object reason;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MassUploadItemResDto &&
    other.id == id &&
    other.status == status &&
    other.priority == priority &&
    other.value == value &&
    other.reason == reason;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (id.hashCode) +
    (status.hashCode) +
    (priority.hashCode) +
    (value.hashCode) +
    (reason.hashCode);

  @override
  String toString() => 'MassUploadItemResDto[id=$id, status=$status, priority=$priority, value=$value, reason=$reason]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'id'] = this.id;
      json[r'status'] = this.status;
      json[r'priority'] = this.priority;
      json[r'value'] = this.value;
      json[r'reason'] = this.reason;
    return json;
  }

  /// Returns a new [MassUploadItemResDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static MassUploadItemResDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "MassUploadItemResDto[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "MassUploadItemResDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return MassUploadItemResDto(
        id: mapValueOfType<String>(json, r'id')!,
        status: MassUploadItemResDtoStatusEnum.fromJson(json[r'status'])!,
        priority: num.parse('${json[r'priority']}'),
        value: mapValueOfType<Object>(json, r'value')!,
        reason: mapValueOfType<Object>(json, r'reason')!,
      );
    }
    return null;
  }

  static List<MassUploadItemResDto> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <MassUploadItemResDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = MassUploadItemResDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, MassUploadItemResDto> mapFromJson(dynamic json) {
    final map = <String, MassUploadItemResDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = MassUploadItemResDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of MassUploadItemResDto-objects as value to a dart map
  static Map<String, List<MassUploadItemResDto>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<MassUploadItemResDto>>{};
    if (json is Map && json.isNotEmpty) {
      // ignore: parameter_assignments
      json = json.cast<String, dynamic>();
      for (final entry in json.entries) {
        map[entry.key] = MassUploadItemResDto.listFromJson(entry.value, growable: growable,);
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'status',
    'priority',
    'value',
    'reason',
  };
}


class MassUploadItemResDtoStatusEnum {
  /// Instantiate a new enum with the provided [value].
  const MassUploadItemResDtoStatusEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const pending = MassUploadItemResDtoStatusEnum._(r'pending');
  static const success = MassUploadItemResDtoStatusEnum._(r'success');
  static const error = MassUploadItemResDtoStatusEnum._(r'error');

  /// List of all possible values in this [enum][MassUploadItemResDtoStatusEnum].
  static const values = <MassUploadItemResDtoStatusEnum>[
    pending,
    success,
    error,
  ];

  static MassUploadItemResDtoStatusEnum? fromJson(dynamic value) => MassUploadItemResDtoStatusEnumTypeTransformer().decode(value);

  static List<MassUploadItemResDtoStatusEnum> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <MassUploadItemResDtoStatusEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = MassUploadItemResDtoStatusEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [MassUploadItemResDtoStatusEnum] to String,
/// and [decode] dynamic data back to [MassUploadItemResDtoStatusEnum].
class MassUploadItemResDtoStatusEnumTypeTransformer {
  factory MassUploadItemResDtoStatusEnumTypeTransformer() => _instance ??= const MassUploadItemResDtoStatusEnumTypeTransformer._();

  const MassUploadItemResDtoStatusEnumTypeTransformer._();

  String encode(MassUploadItemResDtoStatusEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a MassUploadItemResDtoStatusEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  MassUploadItemResDtoStatusEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data) {
        case r'pending': return MassUploadItemResDtoStatusEnum.pending;
        case r'success': return MassUploadItemResDtoStatusEnum.success;
        case r'error': return MassUploadItemResDtoStatusEnum.error;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [MassUploadItemResDtoStatusEnumTypeTransformer] instance.
  static MassUploadItemResDtoStatusEnumTypeTransformer? _instance;
}


