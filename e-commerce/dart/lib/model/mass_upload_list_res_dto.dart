//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class MassUploadListResDto {
  /// Returns a new [MassUploadListResDto] instance.
  MassUploadListResDto({
    this.data = const [],
    this.total = 0,
    this.pageIndex = 1,
    this.pageSize = 10,
  });

  List<MassUploadDto> data;

  num total;

  num pageIndex;

  num pageSize;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MassUploadListResDto &&
    _deepEquality.equals(other.data, data) &&
    other.total == total &&
    other.pageIndex == pageIndex &&
    other.pageSize == pageSize;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (data.hashCode) +
    (total.hashCode) +
    (pageIndex.hashCode) +
    (pageSize.hashCode);

  @override
  String toString() => 'MassUploadListResDto[data=$data, total=$total, pageIndex=$pageIndex, pageSize=$pageSize]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'data'] = this.data;
      json[r'total'] = this.total;
      json[r'pageIndex'] = this.pageIndex;
      json[r'pageSize'] = this.pageSize;
    return json;
  }

  /// Returns a new [MassUploadListResDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static MassUploadListResDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "MassUploadListResDto[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "MassUploadListResDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return MassUploadListResDto(
        data: MassUploadDto.listFromJson(json[r'data']),
        total: num.parse('${json[r'total']}'),
        pageIndex: num.parse('${json[r'pageIndex']}'),
        pageSize: num.parse('${json[r'pageSize']}'),
      );
    }
    return null;
  }

  static List<MassUploadListResDto> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <MassUploadListResDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = MassUploadListResDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, MassUploadListResDto> mapFromJson(dynamic json) {
    final map = <String, MassUploadListResDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = MassUploadListResDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of MassUploadListResDto-objects as value to a dart map
  static Map<String, List<MassUploadListResDto>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<MassUploadListResDto>>{};
    if (json is Map && json.isNotEmpty) {
      // ignore: parameter_assignments
      json = json.cast<String, dynamic>();
      for (final entry in json.entries) {
        map[entry.key] = MassUploadListResDto.listFromJson(entry.value, growable: growable,);
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'data',
    'total',
    'pageIndex',
    'pageSize',
  };
}

