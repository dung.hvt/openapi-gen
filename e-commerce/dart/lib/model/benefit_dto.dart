//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class BenefitDto {
  /// Returns a new [BenefitDto] instance.
  BenefitDto({
    required this.id,
    required this.name,
    this.orgId,
  });

  String id;

  String name;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  Object? orgId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is BenefitDto &&
    other.id == id &&
    other.name == name &&
    other.orgId == orgId;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (id.hashCode) +
    (name.hashCode) +
    (orgId == null ? 0 : orgId!.hashCode);

  @override
  String toString() => 'BenefitDto[id=$id, name=$name, orgId=$orgId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'id'] = this.id;
      json[r'name'] = this.name;
    if (this.orgId != null) {
      json[r'orgId'] = this.orgId;
    } else {
      json[r'orgId'] = null;
    }
    return json;
  }

  /// Returns a new [BenefitDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static BenefitDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "BenefitDto[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "BenefitDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return BenefitDto(
        id: mapValueOfType<String>(json, r'id')!,
        name: mapValueOfType<String>(json, r'name')!,
        orgId: mapValueOfType<Object>(json, r'orgId'),
      );
    }
    return null;
  }

  static List<BenefitDto> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <BenefitDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = BenefitDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, BenefitDto> mapFromJson(dynamic json) {
    final map = <String, BenefitDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BenefitDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of BenefitDto-objects as value to a dart map
  static Map<String, List<BenefitDto>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<BenefitDto>>{};
    if (json is Map && json.isNotEmpty) {
      // ignore: parameter_assignments
      json = json.cast<String, dynamic>();
      for (final entry in json.entries) {
        map[entry.key] = BenefitDto.listFromJson(entry.value, growable: growable,);
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'name',
  };
}

