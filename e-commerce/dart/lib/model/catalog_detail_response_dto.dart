//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CatalogDetailResponseDto {
  /// Returns a new [CatalogDetailResponseDto] instance.
  CatalogDetailResponseDto({
    required this.description,
    required this.availableStock,
    this.categories = const [],
    required this.eVoucherTag,
    required this.productType,
    required this.redemptionInstruction,
    required this.termAndCondition,
    required this.id,
    required this.currency,
    required this.name,
    required this.mainImage,
    this.otherImages = const [],
    required this.basePrice,
    required this.organizationId,
    required this.organizationName,
    required this.organizationStatus,
    this.calculatedCampaigns = const [],
  });

  String description;

  num availableStock;

  List<CategoryDto> categories;

  Object eVoucherTag;

  CatalogDetailResponseDtoProductTypeEnum productType;

  Object redemptionInstruction;

  Object termAndCondition;

  String id;

  String currency;

  String name;

  String mainImage;

  List<String> otherImages;

  num basePrice;

  String organizationId;

  String organizationName;

  CatalogDetailResponseDtoOrganizationStatusEnum organizationStatus;

  List<CalculatedCampaignResponseDto> calculatedCampaigns;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CatalogDetailResponseDto &&
    other.description == description &&
    other.availableStock == availableStock &&
    _deepEquality.equals(other.categories, categories) &&
    other.eVoucherTag == eVoucherTag &&
    other.productType == productType &&
    other.redemptionInstruction == redemptionInstruction &&
    other.termAndCondition == termAndCondition &&
    other.id == id &&
    other.currency == currency &&
    other.name == name &&
    other.mainImage == mainImage &&
    _deepEquality.equals(other.otherImages, otherImages) &&
    other.basePrice == basePrice &&
    other.organizationId == organizationId &&
    other.organizationName == organizationName &&
    other.organizationStatus == organizationStatus &&
    _deepEquality.equals(other.calculatedCampaigns, calculatedCampaigns);

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (description.hashCode) +
    (availableStock.hashCode) +
    (categories.hashCode) +
    (eVoucherTag.hashCode) +
    (productType.hashCode) +
    (redemptionInstruction.hashCode) +
    (termAndCondition.hashCode) +
    (id.hashCode) +
    (currency.hashCode) +
    (name.hashCode) +
    (mainImage.hashCode) +
    (otherImages.hashCode) +
    (basePrice.hashCode) +
    (organizationId.hashCode) +
    (organizationName.hashCode) +
    (organizationStatus.hashCode) +
    (calculatedCampaigns.hashCode);

  @override
  String toString() => 'CatalogDetailResponseDto[description=$description, availableStock=$availableStock, categories=$categories, eVoucherTag=$eVoucherTag, productType=$productType, redemptionInstruction=$redemptionInstruction, termAndCondition=$termAndCondition, id=$id, currency=$currency, name=$name, mainImage=$mainImage, otherImages=$otherImages, basePrice=$basePrice, organizationId=$organizationId, organizationName=$organizationName, organizationStatus=$organizationStatus, calculatedCampaigns=$calculatedCampaigns]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
      json[r'description'] = this.description;
      json[r'availableStock'] = this.availableStock;
      json[r'categories'] = this.categories;
      json[r'eVoucherTag'] = this.eVoucherTag;
      json[r'productType'] = this.productType;
      json[r'redemptionInstruction'] = this.redemptionInstruction;
      json[r'termAndCondition'] = this.termAndCondition;
      json[r'id'] = this.id;
      json[r'currency'] = this.currency;
      json[r'name'] = this.name;
      json[r'mainImage'] = this.mainImage;
      json[r'otherImages'] = this.otherImages;
      json[r'basePrice'] = this.basePrice;
      json[r'organizationId'] = this.organizationId;
      json[r'organizationName'] = this.organizationName;
      json[r'organizationStatus'] = this.organizationStatus;
      json[r'calculatedCampaigns'] = this.calculatedCampaigns;
    return json;
  }

  /// Returns a new [CatalogDetailResponseDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static CatalogDetailResponseDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "CatalogDetailResponseDto[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "CatalogDetailResponseDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return CatalogDetailResponseDto(
        description: mapValueOfType<String>(json, r'description')!,
        availableStock: num.parse('${json[r'availableStock']}'),
        categories: CategoryDto.listFromJson(json[r'categories']),
        eVoucherTag: mapValueOfType<Object>(json, r'eVoucherTag')!,
        productType: CatalogDetailResponseDtoProductTypeEnum.fromJson(json[r'productType'])!,
        redemptionInstruction: mapValueOfType<Object>(json, r'redemptionInstruction')!,
        termAndCondition: mapValueOfType<Object>(json, r'termAndCondition')!,
        id: mapValueOfType<String>(json, r'id')!,
        currency: mapValueOfType<String>(json, r'currency')!,
        name: mapValueOfType<String>(json, r'name')!,
        mainImage: mapValueOfType<String>(json, r'mainImage')!,
        otherImages: json[r'otherImages'] is Iterable
            ? (json[r'otherImages'] as Iterable).cast<String>().toList(growable: false)
            : const [],
        basePrice: num.parse('${json[r'basePrice']}'),
        organizationId: mapValueOfType<String>(json, r'organizationId')!,
        organizationName: mapValueOfType<String>(json, r'organizationName')!,
        organizationStatus: CatalogDetailResponseDtoOrganizationStatusEnum.fromJson(json[r'organizationStatus'])!,
        calculatedCampaigns: CalculatedCampaignResponseDto.listFromJson(json[r'calculatedCampaigns']),
      );
    }
    return null;
  }

  static List<CatalogDetailResponseDto> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CatalogDetailResponseDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CatalogDetailResponseDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, CatalogDetailResponseDto> mapFromJson(dynamic json) {
    final map = <String, CatalogDetailResponseDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CatalogDetailResponseDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of CatalogDetailResponseDto-objects as value to a dart map
  static Map<String, List<CatalogDetailResponseDto>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<CatalogDetailResponseDto>>{};
    if (json is Map && json.isNotEmpty) {
      // ignore: parameter_assignments
      json = json.cast<String, dynamic>();
      for (final entry in json.entries) {
        map[entry.key] = CatalogDetailResponseDto.listFromJson(entry.value, growable: growable,);
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'description',
    'availableStock',
    'categories',
    'eVoucherTag',
    'productType',
    'redemptionInstruction',
    'termAndCondition',
    'id',
    'currency',
    'name',
    'mainImage',
    'otherImages',
    'basePrice',
    'organizationId',
    'organizationName',
    'organizationStatus',
    'calculatedCampaigns',
  };
}


class CatalogDetailResponseDtoProductTypeEnum {
  /// Instantiate a new enum with the provided [value].
  const CatalogDetailResponseDtoProductTypeEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const main = CatalogDetailResponseDtoProductTypeEnum._(r'main');
  static const addons = CatalogDetailResponseDtoProductTypeEnum._(r'addons');
  static const eCommerce = CatalogDetailResponseDtoProductTypeEnum._(r'e_commerce');
  static const program = CatalogDetailResponseDtoProductTypeEnum._(r'program');
  static const eVoucher = CatalogDetailResponseDtoProductTypeEnum._(r'e_voucher');

  /// List of all possible values in this [enum][CatalogDetailResponseDtoProductTypeEnum].
  static const values = <CatalogDetailResponseDtoProductTypeEnum>[
    main,
    addons,
    eCommerce,
    program,
    eVoucher,
  ];

  static CatalogDetailResponseDtoProductTypeEnum? fromJson(dynamic value) => CatalogDetailResponseDtoProductTypeEnumTypeTransformer().decode(value);

  static List<CatalogDetailResponseDtoProductTypeEnum> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CatalogDetailResponseDtoProductTypeEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CatalogDetailResponseDtoProductTypeEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [CatalogDetailResponseDtoProductTypeEnum] to String,
/// and [decode] dynamic data back to [CatalogDetailResponseDtoProductTypeEnum].
class CatalogDetailResponseDtoProductTypeEnumTypeTransformer {
  factory CatalogDetailResponseDtoProductTypeEnumTypeTransformer() => _instance ??= const CatalogDetailResponseDtoProductTypeEnumTypeTransformer._();

  const CatalogDetailResponseDtoProductTypeEnumTypeTransformer._();

  String encode(CatalogDetailResponseDtoProductTypeEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a CatalogDetailResponseDtoProductTypeEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  CatalogDetailResponseDtoProductTypeEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data) {
        case r'main': return CatalogDetailResponseDtoProductTypeEnum.main;
        case r'addons': return CatalogDetailResponseDtoProductTypeEnum.addons;
        case r'e_commerce': return CatalogDetailResponseDtoProductTypeEnum.eCommerce;
        case r'program': return CatalogDetailResponseDtoProductTypeEnum.program;
        case r'e_voucher': return CatalogDetailResponseDtoProductTypeEnum.eVoucher;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [CatalogDetailResponseDtoProductTypeEnumTypeTransformer] instance.
  static CatalogDetailResponseDtoProductTypeEnumTypeTransformer? _instance;
}



class CatalogDetailResponseDtoOrganizationStatusEnum {
  /// Instantiate a new enum with the provided [value].
  const CatalogDetailResponseDtoOrganizationStatusEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const verified = CatalogDetailResponseDtoOrganizationStatusEnum._(r'verified');
  static const unverified = CatalogDetailResponseDtoOrganizationStatusEnum._(r'unverified');

  /// List of all possible values in this [enum][CatalogDetailResponseDtoOrganizationStatusEnum].
  static const values = <CatalogDetailResponseDtoOrganizationStatusEnum>[
    verified,
    unverified,
  ];

  static CatalogDetailResponseDtoOrganizationStatusEnum? fromJson(dynamic value) => CatalogDetailResponseDtoOrganizationStatusEnumTypeTransformer().decode(value);

  static List<CatalogDetailResponseDtoOrganizationStatusEnum> listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CatalogDetailResponseDtoOrganizationStatusEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CatalogDetailResponseDtoOrganizationStatusEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [CatalogDetailResponseDtoOrganizationStatusEnum] to String,
/// and [decode] dynamic data back to [CatalogDetailResponseDtoOrganizationStatusEnum].
class CatalogDetailResponseDtoOrganizationStatusEnumTypeTransformer {
  factory CatalogDetailResponseDtoOrganizationStatusEnumTypeTransformer() => _instance ??= const CatalogDetailResponseDtoOrganizationStatusEnumTypeTransformer._();

  const CatalogDetailResponseDtoOrganizationStatusEnumTypeTransformer._();

  String encode(CatalogDetailResponseDtoOrganizationStatusEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a CatalogDetailResponseDtoOrganizationStatusEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  CatalogDetailResponseDtoOrganizationStatusEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data) {
        case r'verified': return CatalogDetailResponseDtoOrganizationStatusEnum.verified;
        case r'unverified': return CatalogDetailResponseDtoOrganizationStatusEnum.unverified;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [CatalogDetailResponseDtoOrganizationStatusEnumTypeTransformer] instance.
  static CatalogDetailResponseDtoOrganizationStatusEnumTypeTransformer? _instance;
}


