//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

library openapi.api;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'api_client.dart';
part 'api_helper.dart';
part 'api_exception.dart';
part 'auth/authentication.dart';
part 'auth/api_key_auth.dart';
part 'auth/oauth.dart';
part 'auth/http_basic_auth.dart';
part 'auth/http_bearer_auth.dart';

part 'api/benefits_api.dart';
part 'api/catalogs_api.dart';
part 'api/default_api.dart';
part 'api/inventories_api.dart';

part 'model/benefit_detail_dto.dart';
part 'model/benefit_dto.dart';
part 'model/benefit_list_response_dto.dart';
part 'model/calculated_campaign_response_dto.dart';
part 'model/catalog_detail_response_dto.dart';
part 'model/catalog_item_base_dto.dart';
part 'model/catalog_item_by_org_dto.dart';
part 'model/catalog_list_by_org_response_dto.dart';
part 'model/catalog_list_response_dto.dart';
part 'model/category_dto.dart';
part 'model/created_e_voucher_code_res_dto.dart';
part 'model/created_inventory_response_dto.dart';
part 'model/created_response_dto.dart';
part 'model/e_voucher_code_dto.dart';
part 'model/e_voucher_code_list_response_dto.dart';
part 'model/get_mass_upload_detail_res_dto.dart';
part 'model/healthz_controller_check200_response.dart';
part 'model/healthz_controller_check200_response_info_value.dart';
part 'model/healthz_controller_check503_response.dart';
part 'model/mass_upload_dto.dart';
part 'model/mass_upload_item_res_dto.dart';
part 'model/mass_upload_list_res_dto.dart';
part 'model/product_inventory_detail_dto.dart';
part 'model/product_inventory_dto.dart';
part 'model/product_inventory_list_response_dto.dart';
part 'model/simple_catalog_item_dto.dart';
part 'model/simple_catalog_list_response_dto.dart';


/// An [ApiClient] instance that uses the default values obtained from
/// the OpenAPI specification file.
var defaultApiClient = ApiClient();

const _delimiters = {'csv': ',', 'ssv': ' ', 'tsv': '\t', 'pipes': '|'};
const _dateEpochMarker = 'epoch';
const _deepEquality = DeepCollectionEquality();
final _dateFormatter = DateFormat('yyyy-MM-dd');
final _regList = RegExp(r'^List<(.*)>$');
final _regSet = RegExp(r'^Set<(.*)>$');
final _regMap = RegExp(r'^Map<String,(.*)>$');

bool _isEpochMarker(String? pattern) => pattern == _dateEpochMarker || pattern == '/$_dateEpochMarker/';
