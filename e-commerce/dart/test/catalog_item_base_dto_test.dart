//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for CatalogItemBaseDto
void main() {
  // final instance = CatalogItemBaseDto();

  group('test CatalogItemBaseDto', () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // String currency
    test('to test the property `currency`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String mainImage
    test('to test the property `mainImage`', () async {
      // TODO
    });

    // List<String> otherImages (default value: const [])
    test('to test the property `otherImages`', () async {
      // TODO
    });

    // num basePrice
    test('to test the property `basePrice`', () async {
      // TODO
    });

    // String organizationId
    test('to test the property `organizationId`', () async {
      // TODO
    });

    // String organizationName
    test('to test the property `organizationName`', () async {
      // TODO
    });

    // String organizationStatus
    test('to test the property `organizationStatus`', () async {
      // TODO
    });

    // List<CalculatedCampaignResponseDto> calculatedCampaigns (default value: const [])
    test('to test the property `calculatedCampaigns`', () async {
      // TODO
    });

    // Object eVoucherTag
    test('to test the property `eVoucherTag`', () async {
      // TODO
    });

    // String productType
    test('to test the property `productType`', () async {
      // TODO
    });


  });

}
