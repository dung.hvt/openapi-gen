//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for MassUploadListResDto
void main() {
  // final instance = MassUploadListResDto();

  group('test MassUploadListResDto', () {
    // List<MassUploadDto> data (default value: const [])
    test('to test the property `data`', () async {
      // TODO
    });

    // num total (default value: 0)
    test('to test the property `total`', () async {
      // TODO
    });

    // num pageIndex (default value: 1)
    test('to test the property `pageIndex`', () async {
      // TODO
    });

    // num pageSize (default value: 10)
    test('to test the property `pageSize`', () async {
      // TODO
    });


  });

}
