//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for CatalogDetailResponseDto
void main() {
  // final instance = CatalogDetailResponseDto();

  group('test CatalogDetailResponseDto', () {
    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // num availableStock
    test('to test the property `availableStock`', () async {
      // TODO
    });

    // List<CategoryDto> categories (default value: const [])
    test('to test the property `categories`', () async {
      // TODO
    });

    // Object eVoucherTag
    test('to test the property `eVoucherTag`', () async {
      // TODO
    });

    // String productType
    test('to test the property `productType`', () async {
      // TODO
    });

    // Object redemptionInstruction
    test('to test the property `redemptionInstruction`', () async {
      // TODO
    });

    // Object termAndCondition
    test('to test the property `termAndCondition`', () async {
      // TODO
    });

    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // String currency
    test('to test the property `currency`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String mainImage
    test('to test the property `mainImage`', () async {
      // TODO
    });

    // List<String> otherImages (default value: const [])
    test('to test the property `otherImages`', () async {
      // TODO
    });

    // num basePrice
    test('to test the property `basePrice`', () async {
      // TODO
    });

    // String organizationId
    test('to test the property `organizationId`', () async {
      // TODO
    });

    // String organizationName
    test('to test the property `organizationName`', () async {
      // TODO
    });

    // String organizationStatus
    test('to test the property `organizationStatus`', () async {
      // TODO
    });

    // List<CalculatedCampaignResponseDto> calculatedCampaigns (default value: const [])
    test('to test the property `calculatedCampaigns`', () async {
      // TODO
    });


  });

}
