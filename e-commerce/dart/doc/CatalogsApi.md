# openapi.api.CatalogsApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://api.dev.getcare.io/v1/e-commerce*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogControllerGetCatalogDetail**](CatalogsApi.md#catalogcontrollergetcatalogdetail) | **GET** /catalogs/{productId} | 
[**catalogControllerGetCatalogs**](CatalogsApi.md#catalogcontrollergetcatalogs) | **GET** /catalogs | 


# **catalogControllerGetCatalogDetail**
> CatalogDetailResponseDto catalogControllerGetCatalogDetail(productId)



### Example
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP Bearer authorization: bearer
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken(yourTokenGeneratorFunction);

final api_instance = CatalogsApi();
final productId = productId_example; // String | 

try {
    final result = api_instance.catalogControllerGetCatalogDetail(productId);
    print(result);
} catch (e) {
    print('Exception when calling CatalogsApi->catalogControllerGetCatalogDetail: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **String**|  | 

### Return type

[**CatalogDetailResponseDto**](CatalogDetailResponseDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogControllerGetCatalogs**
> CatalogListResponseDto catalogControllerGetCatalogs(pageIndex, pageSize, currentCountry, parentCategoryId, categoryId, keyword, productIds, categoryIds, productTypes, sortBy)



### Example
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP Bearer authorization: bearer
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken(yourTokenGeneratorFunction);

final api_instance = CatalogsApi();
final pageIndex = 8.14; // num | 
final pageSize = 8.14; // num | 
final currentCountry = currentCountry_example; // String | 
final parentCategoryId = parentCategoryId_example; // String | 
final categoryId = categoryId_example; // String | 
final keyword = keyword_example; // String | 
final productIds = []; // List<String> | 
final categoryIds = []; // List<String> | 
final productTypes = []; // List<String> | 
final sortBy = sortBy_example; // String | 

try {
    final result = api_instance.catalogControllerGetCatalogs(pageIndex, pageSize, currentCountry, parentCategoryId, categoryId, keyword, productIds, categoryIds, productTypes, sortBy);
    print(result);
} catch (e) {
    print('Exception when calling CatalogsApi->catalogControllerGetCatalogs: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pageIndex** | **num**|  | [default to 1]
 **pageSize** | **num**|  | [default to 10]
 **currentCountry** | **String**|  | 
 **parentCategoryId** | **String**|  | [optional] 
 **categoryId** | **String**|  | [optional] 
 **keyword** | **String**|  | [optional] 
 **productIds** | [**List<String>**](String.md)|  | [optional] [default to const []]
 **categoryIds** | [**List<String>**](String.md)|  | [optional] [default to const []]
 **productTypes** | [**List<String>**](String.md)|  | [optional] [default to const []]
 **sortBy** | **String**|  | [optional] 

### Return type

[**CatalogListResponseDto**](CatalogListResponseDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

