# openapi.model.MassUploadListResDto

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List<MassUploadDto>**](MassUploadDto.md) |  | [default to const []]
**total** | **num** |  | [default to 0]
**pageIndex** | **num** |  | [default to 1]
**pageSize** | **num** |  | [default to 10]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


