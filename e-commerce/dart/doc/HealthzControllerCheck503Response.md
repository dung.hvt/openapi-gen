# openapi.model.HealthzControllerCheck503Response

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | [optional] 
**info** | [**Map<String, HealthzControllerCheck200ResponseInfoValue>**](HealthzControllerCheck200ResponseInfoValue.md) |  | [optional] [default to const {}]
**error** | [**Map<String, HealthzControllerCheck200ResponseInfoValue>**](HealthzControllerCheck200ResponseInfoValue.md) |  | [optional] [default to const {}]
**details** | [**Map<String, HealthzControllerCheck200ResponseInfoValue>**](HealthzControllerCheck200ResponseInfoValue.md) |  | [optional] [default to const {}]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


