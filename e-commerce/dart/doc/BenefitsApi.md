# openapi.api.BenefitsApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://api.dev.getcare.io/v1/e-commerce*

Method | HTTP request | Description
------------- | ------------- | -------------
[**benefitControllerGetBenefitDetail**](BenefitsApi.md#benefitcontrollergetbenefitdetail) | **GET** /benefits/{id} | 


# **benefitControllerGetBenefitDetail**
> benefitControllerGetBenefitDetail(id)



### Example
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP Bearer authorization: bearer
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken(yourTokenGeneratorFunction);

final api_instance = BenefitsApi();
final id = id_example; // String | 

try {
    api_instance.benefitControllerGetBenefitDetail(id);
} catch (e) {
    print('Exception when calling BenefitsApi->benefitControllerGetBenefitDetail: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

