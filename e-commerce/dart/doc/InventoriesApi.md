# openapi.api.InventoriesApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://api.dev.getcare.io/v1/e-commerce*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productInventoryControllerGetProductInventoryDetail**](InventoriesApi.md#productinventorycontrollergetproductinventorydetail) | **GET** /inventories/products/{productId} | 
[**productInventoryControllerGetProductInventoryList**](InventoriesApi.md#productinventorycontrollergetproductinventorylist) | **GET** /inventories/products | 


# **productInventoryControllerGetProductInventoryDetail**
> productInventoryControllerGetProductInventoryDetail(productId)



### Example
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP Bearer authorization: bearer
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken(yourTokenGeneratorFunction);

final api_instance = InventoriesApi();
final productId = productId_example; // String | 

try {
    api_instance.productInventoryControllerGetProductInventoryDetail(productId);
} catch (e) {
    print('Exception when calling InventoriesApi->productInventoryControllerGetProductInventoryDetail: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **productInventoryControllerGetProductInventoryList**
> productInventoryControllerGetProductInventoryList(pageIndex, pageSize, productIds)



### Example
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP Bearer authorization: bearer
// Case 1. Use String Token
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken('YOUR_ACCESS_TOKEN');
// Case 2. Use Function which generate token.
// String yourTokenGeneratorFunction() { ... }
//defaultApiClient.getAuthentication<HttpBearerAuth>('bearer').setAccessToken(yourTokenGeneratorFunction);

final api_instance = InventoriesApi();
final pageIndex = 8.14; // num | 
final pageSize = 8.14; // num | 
final productIds = []; // List<String> | 

try {
    api_instance.productInventoryControllerGetProductInventoryList(pageIndex, pageSize, productIds);
} catch (e) {
    print('Exception when calling InventoriesApi->productInventoryControllerGetProductInventoryList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pageIndex** | **num**|  | [default to 1]
 **pageSize** | **num**|  | [default to 10]
 **productIds** | [**List<String>**](String.md)|  | [optional] [default to const []]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

