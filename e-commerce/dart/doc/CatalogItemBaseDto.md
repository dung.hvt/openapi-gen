# openapi.model.CatalogItemBaseDto

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**currency** | **String** |  | 
**name** | **String** |  | 
**mainImage** | **String** |  | 
**otherImages** | **List<String>** |  | [default to const []]
**basePrice** | **num** |  | 
**organizationId** | **String** |  | 
**organizationName** | **String** |  | 
**organizationStatus** | **String** |  | 
**calculatedCampaigns** | [**List<CalculatedCampaignResponseDto>**](CalculatedCampaignResponseDto.md) |  | [default to const []]
**eVoucherTag** | [**Object**](.md) |  | 
**productType** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


