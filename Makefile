OPENAPI_GEN := docker run --rm -v "${PWD}:/local" openapitools/openapi-generator-cli generate

gen_ecommerce:
	$(OPENAPI_GEN) -i https://api.dev.getcare.io/v1/e-commerce/apidoc-json/ -g typescript-axios -o /local/e-commerce/typescript --additional-properties=withSeparateModelsAndApi=true,apiPackage=api,modelPackage=models,supportsES6=true
	$(OPENAPI_GEN) -i https://api.dev.getcare.io/v1/e-commerce/apidoc-json/ -g dart -o /local/e-commerce/dart
